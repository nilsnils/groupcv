$.ajax({
    url: "/data",
    dataType:'json',
    cache: false,
    success: function(data){
        console.log(data);

        /* Header information from json */
        $("#header_titel").html(data.PersonalInformation.WorkTitle);
        $("#personal_name").html(data.PersonalInformation.Name);
        $("#header_email").html(data.PersonalInformation.ContactInformation.Mail);
        $("#header_phone").html(data.PersonalInformation.ContactInformation.Phone);
        
        /* Firstpage/section1 information from json */
        $("#profilePic").attr("src", data.PersonalInformation.ProfilePicture);
        $("#titel").html(data.PersonalInformation.WorkTitle);
        $("#email").html(data.PersonalInformation.ContactInformation.Mail);
        $("#phone").html(data.PersonalInformation.ContactInformation.Phone);

        /* Secondpage/section2 information from json */
        // $("#gymnasie").html(data.PersonalInformation.ContactInformation.Phone);
        $("#gymnasie").html(data.Educational.Gymnasie.Name);
        $("#startYear").html(data.Educational.Gymnasie.StartYear);
        $("#endYear").html(data.Educational.Gymnasie.EndYear);
        $("#program").html(data.Educational.Gymnasie.Program);
        //University
        $("#university").html(data.Educational.University.Name);
        $("#uni_startYear").html(data.Educational.University.StartYear);
        $("#uni_endYear").html(data.Educational.University.EndYear);
        $("#uni_program").html(data.Educational.University.Program);
        //Additinal courses
        $("#add_schoolName").html(data.Educational.AdditionalEducation.Name);
        $("#add_startYear").html(data.Educational.AdditionalEducation.StartYear);
        $("#add_endYear").html(data.Educational.AdditionalEducation.EndYear);
        $("#add_courseName").html(data.Educational.AdditionalEducation.CourseName)
        
        $("#skills").html(data.Educational.Skills.ProgrammingLanguage); //Array?
        $("#additionalSkills").html(data.Educational.Skills.AdditionalSkills);//Array?
        $("#workExp").html(data.WorkExperience);
        
        /* Thirdpage/section3 information from json */
        

        /* Fourthpage/section4 information from json */
        $("#email2").html(data.PersonalInformation.ContactInformation.Mail);
        $("#phone2").html(data.PersonalInformation.ContactInformation.Phone);

        $("#City").html(data.PersonalInformation.City);
        $("#Country").html(data.PersonalInformation.Country);
    },
    error: function (err) {
        console.log(err);
    }
 });